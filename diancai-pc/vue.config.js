// const proxy = require('http-proxy-middleware');

module.exports = {
    devServer: {
      // host: '0.0.0.0',
        // port: 8080,
        // host: 'localhost', //target host
	      // port: 8080,
        open:true,
        proxy: {  //配置跨域
          [process.env.VUE_APP_BASE_API]: {
            // target: 'http://127.0.0.1:1111',
            target: 'http://47.96.162.20:1111',  
            changOrigin: true,  //允许跨域
            logLevel: 'debug',  //显示代理地址
            pathRewrite: {
              ['^' + process.env.VUE_APP_BASE_API]: '' 
              // '^/api': '' 
            }
          },
        }
    },
}