import axios from 'axios'
import qs from 'qs'

// //2.全局配置
// axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v2/'
axios.defaults.baseURL = process.env.VUE_APP_BASE_API
// axios.defaults.baseURL = '/api'
// axios.defaults.baseURL = 'http://47.96.162.20:1111'
// axios.defaults.baseURL = 'http://127.0.0.1:1111' 
// 添加请求拦截器
axios.interceptors.request.use(function (config) { 
  let token = localStorage.getItem('token')
  config.headers['token'] = token
  // 拦截所有请求  发送之前就加一些东西
  config.headers['Content-Type'] = 'application/x-www-form-urlencoded'

  // 在发送请求
  return config;
}, function (error) {
  // 对请求错误做些什么12
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // console.log(response)
  // if (response.data.meta.msg == 'TOKEN有误')
  // {
  //     Message({
  //         showClose: true,
  //         message: "糟糕TOKEN已过期请重新登录",
  //         type: "error"
  //     });
  //     return router.push({path:'/login'})
  // }
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error,111);
});


export default axios
