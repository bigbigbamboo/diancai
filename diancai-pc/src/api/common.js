



import axios from './http'
import qs from 'qs'

const uploadApi = file => {
    return axios.post('/test/upload', file).then(res => res.data)
} 



export  {
    uploadApi
}