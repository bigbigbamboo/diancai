import axios from './http'
import qs from 'qs'


 
const queryDishOrderApi = (params) => {
    return axios.post('/dishs/queryDishOrder',qs.stringify(params)).then(res => res.data)
} 


const queryDishsDetailApi = (params) => {
    return axios.post('/dishs/queryDishsDetail',qs.stringify(params)).then(res => res.data)
} 

const updateDishOrderApi = (params) => {
    return axios.post('/dishs/updateDishsOrder',qs.stringify(params)).then(res => res.data)
} 




export  { 
    queryDishOrderApi, 
    queryDishsDetailApi,
    updateDishOrderApi
}


// createTime: "2021-10-14T15:52:16.000Z"
// deskNum: 1
// dinners: 4
// dishList: "11,9;15,14;14,15"
// id: 11
// numList: "1,1;1,1;1,1"
// orderStatus: "0"
// orderUid: "a3214"
// totalPrice: "134"