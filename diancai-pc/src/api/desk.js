import axios from './http'
import qs from 'qs'


 
const createAppCodeApi = (params) => {
    return axios.post('/dish/createAppCode',qs.stringify(params)).then(res => res.data)
} 

const queryAppCodeApi = (params) => {
    return axios.post('/dish/queryAppCode',qs.stringify(params)).then(res => res.data)
} 


 




export  { 
    createAppCodeApi, 
    queryAppCodeApi 
}
 