



import axios from './http'
import qs from 'qs'

const queryAnalysisApi = () => {
    return axios.post('/echarts/analysisApi').then(res => res.data)
} 



export  {
    queryAnalysisApi
}