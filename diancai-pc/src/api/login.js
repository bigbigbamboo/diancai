import axios from './http'
import qs from 'qs'

const loginApi = params => {
    return axios.post('/login', qs.stringify(params)).then(res => res.data)
} 
const registerApi = params => {  
    return axios.post('/register', qs.stringify(params)).then(res => res.data)
}


const menu = () => { 
    return axios.get('auth/menu.php', {}).then(res => res.data)
}



export  {
    loginApi,
    registerApi
}