import axios from './http'
import qs from 'qs'


// 菜品分类
const createCategoryApi = params => {
    return axios.post('/dishCategory/CreateDishesCategory', qs.stringify(params)).then(res => res.data)
} 

const queryCategoryApi = () => { 
    return axios.get('/dishCategory/queryDishesCategory', {}).then(res => res.data)
}

const delCategoryApi = params => {
    return axios.post('/dishCategory/deleteDishesCategory', qs.stringify(params)).then(res => res.data)
}


// 菜品管理
const dishMangeQueryApi = (params) => { 
    return axios.post('/dishsMange/queryDish', qs.stringify(params)).then(res => res.data)
}
const dishMangeCreateApi = params => {
    return axios.post('/dishsMange/createDish', qs.stringify(params)).then(res => res.data)
}
const dishMangeDeleteApi = params => {
    return axios.post('/dishsMange/deleteDish', qs.stringify(params)).then(res => res.data)
}


export  { 
    createCategoryApi,
    queryCategoryApi,
    delCategoryApi,
    dishMangeQueryApi,
    dishMangeCreateApi,
    dishMangeDeleteApi
}