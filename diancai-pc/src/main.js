import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import GoEasy from 'goeasy'

Vue.config.productionTip = false
 

Vue.prototype.goeasy = GoEasy.getInstance({
  host:"hangzhou.goeasy.io",  //若是新加坡区域：singapore.goeasy.io
  appkey:"BC-ce2f321ee7344a45a0756edb2e163067",
  modules:['pubsub']//根据需要，传入‘pubsub’或'im’，或数组方式同时传入
});

console.log(process.env,'process.env')
if (process.env.NODE_ENV === "development") {
  console.log("开发环境");
}else {
  console.log("生产环境");
}



// 进度条 
import NProgress from 'nprogress'
import 'nprogress/nprogress.css' //这个样式必须引入

// 配置 ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 配置 全局初始化样式
//11
import '@/assets/css/reset.scss';

import animated from 'animate.css' // npm install animate.css --save安装，在引入
Vue.use(animated)
 

// 引入echarts开始
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

 
//进度条
NProgress.inc(0.2)
NProgress.configure({ easing: 'ease', speed: 500, showSpinner: false })
router.beforeEach((to,from,next) => {
  NProgress.start()
  next()
})
router.afterEach(() => {
  NProgress.done()
})

//重复点击菜单报错
import Router from 'vue-router'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}
// 地图开始
    // 引入高德地图
    // 高德地图组件使用
    import VueAMap from 'vue-amap'
    
    Vue.config.productionTip = false
    Vue.use(VueAMap);
    VueAMap.initAMapApiLoader({
      key: '698bf15d9133cfcf97398213ff18c29d', 
      plugin: [
      'AMap.Autocomplete', 
      'AMap.PlaceSearch', // POI搜索插件
      'AMap.Scale', // 右下角缩略图插件 比例尺
      'AMap.OverView', 
      'AMap.ToolBar', // 地图工具条
      'AMap.MapType', 
      'AMap.PolyEditor', 
      'AMap.CircleEditor',// 圆形编辑器插件
      'AMap.Geolocation'// 定位控件，用来获取和展示用户主机所在的经纬度位置
      ],
      // 默认高德 sdk 版本为 1.4.4
      v: '1.4.4'
    });

// 地图结束


import moment from 'moment'
Vue.prototype.$moment = moment
 


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
