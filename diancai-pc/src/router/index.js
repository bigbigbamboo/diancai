import Vue from 'vue'
import VueRouter from 'vue-router'
import Message from 'element-ui'
 //sourceTree测试11122235
Vue.use(VueRouter) 
 
  const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/users/Login.vue')
  },
  {
    path: '/',
    component: () => import('../views/admin/Index.vue'),
    redirect:'/dishMange',
    children:[
      {path: '/dishMange',name: 'dishMange',component: () => import('../views/dishs/dishMange.vue')},
      {path: '/dishCategroy',name: 'dishCategroy',component: () => import('../views/dishs/dishCategroy.vue')},
      {path: '/dishOrders',name: 'order',component: () => import('../views/order/order.vue')},
      {path: '/deskNum',name: 'deskNum',component: () => import('../views/desktop/deskNum.vue')},
      {path: '/analysis',name: 'analysis',component: () => import('../views/analysis/analysis.vue')},
      {path: '/amap',name: 'amap',component: () => import('../views/amap/amap.vue')},
      {path: '*',name: 'fail',component: () => import('../views/404/404.vue')},
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


// 全局前置守卫
// 在Router实例上进行守卫
router.beforeEach((to, from, next) => {
  // 获取token
  const token = localStorage.getItem('token');
  // 有token
  if (token) {
    // 直接放行
    next();
  } else {  // 否则是没有
    // 如果去的是登录页
    if (to.path === '/login') {
      // 直接放行
      next();
    } else {
      // 如果去的是其他页,跳转到登录页
      // Message.error('请登录以后再操作！')
      // 跳转到登录页
      return next({ path: "/login" })
    }
  }
})

export default router
