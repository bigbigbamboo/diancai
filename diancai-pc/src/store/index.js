import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
// 我是store
import order from './module/order'
import analysis from './module/analysis'
import getters from './getters'

export default new Vuex.Store({
  state: {
  },
  getters,
  modules: {
    order,analysis
  }
})
