export default {
    namespaced: true,
    state: { 
        newOrderNum:0
    }, 
    mutations: {
        setOrderNum(state, val) { 
            state.newOrderNum += Number(val)
        },
        clearOrderNum(state, val) { 
            state.newOrderNum =0
        },
    },
    actions: {
        // setUserinfo(context,val) {
        //     context.commit('setUserinfo',payload)
        // }
    },
}