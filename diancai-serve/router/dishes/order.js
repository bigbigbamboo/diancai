const router = require("koa-router")();
const handel = require("../../tool/handel");
const respData = require("../../tool/respData");
const {
  getToken,
  addUrl,
  queryUrl,
  delUrl,
  updateUrl,
} = require("../../tool/getToken");
const { taketoken } = require("../../token/jwt");
const { Auth } = require("../../token/auth");
const { checkParams, checkLogin } = require("../../tool/checkParams");
const { GetTime } = require("../../tool/getTime");
const { query } = require("../../MYSQL/query");
 

router.post("/dishs/createDishOrder", async (ctx) => {
  // new checkParams(ctx.request.body).checkParamIsNull() 
  const {
    deskNum,
    dinners,
    dishList,
    totalPrice,
    numList,
    orderUid,
    orderStatus,
  } = ctx.request.body;
  const querySql = `select * from Dishs_order where orderUid=?`;
  const queryParam = [orderUid];
  const sql_data = await query(querySql, queryParam);

  if (orderUid && sql_data.length > 0) {
    const d = sql_data[0].dishList + ";";
    const n = sql_data[0].numList + ";";
    const updateSql =
      "update Dishs_order set totalPrice=?,numList=?,dishList=?,totalPrice=? where orderUid=?";
    const updateParams = [
      totalPrice,
      n + "" + numList.join(),
      d + "" + dishList.join(),
      totalPrice,
      orderUid,
    ];
    // console.log("修改", updateParams);
    try {
      const update_data = await query(updateSql, updateParams);
      const createTime = new GetTime().getTimeFn();
      new respData(ctx, { msg: "加菜成功", orderUid,orderStatus,orderUid,createTime }, 200).respFn();
    } catch (error) {
      console.log(error,11)
      new respData(ctx, "加菜失败", 500).respFn();
    }
  }
  // 增加一条点菜订单
  if (!orderUid) {
    // console.log('dinners',dinners)
    // const orderUid = 'a' + Math.ceil(Math.random()*10000)
    const orderUid = new GetTime().getTime_forOrder() + deskNum + dinners;
    const createTime = new GetTime().getTimeFn();
    const addSql =
      "insert into Dishs_order(deskNum,dinners,dishList,totalPrice,numList,orderUid,orderStatus,createTime) values(?,?,?,?,?,?,?,?)";
    const addParams = [
      deskNum,
      dinners,
      dishList.join(),
      totalPrice,
      numList.join(),
      orderUid,
      orderStatus,
      createTime,
    ];
    // console.log("新增1", addParams);
    const add_data = await query(addSql, addParams);
    try {
      new respData(ctx, { msg: "下单成功", orderUid,orderStatus,orderUid,createTime }, 200).respFn();
    } catch (error) {
      new respData(ctx, "下单失败", 500).respFn();
    }
  }
});

router.post("/dishs/queryDishOrder", async (ctx) => {
  // new checkParams(ctx.request.body).checkParamIsNull()

  const {
    deskNum,
    dinners,
    dishList,
    totalPrice,
    numList,
    orderUid,
    page,
    orderStatus,
  } = ctx.request.body;
  if(!orderUid){ 
    const Sql = `select * from Dishs_order`;
    const all_data = await query(Sql);

    var start = (page - 1) * 10;  
    const querySql = 'select * from Dishs_order limit ' + start + ',10'
    // const querySql = `select * from Dishs_order`;
    const sql_data = await query(querySql);
    new respData(ctx, {list:sql_data,totalPage:all_data.length}, 200).respFn();
  }
  

  if(orderUid){ 
    const querySql = `select * from Dishs_order where orderUid=?`;
    const queryParam = [orderUid]
    const singel_data = await query(querySql,queryParam);
    new respData(ctx, singel_data, 200).respFn();
  }

});

router.post("/dishs/queryDishsDetail", async (ctx) => {
  // new checkParams(ctx.request.body).checkParamIsNull()

  const { orderUid } = ctx.request.body;
  // 查询菜品
  const dishsSql = `select * from Dishes_manage`;
  const dishs_data = await query(dishsSql);
  // 查询当前订单详细数据
  const querySql = `select * from Dishs_order where orderUid=?`;
  const queryParam = [orderUid];
  const sql_data = await query(querySql, queryParam);
  const dishList = sql_data[0].dishList;
  const numList = sql_data[0].numList;

  const respArr = [];
  dishList.split(";").forEach((ele) => {
    respArr.push(ele.split(","));
  });
  respArr.forEach((ele, idx) => {
    ele.forEach((item, index) => {
      obj = dishs_data.find((sub) => {
        return sub.id === Number(item);
      });
      ele[index] = { 
        dishName: obj.dishName, 
        num: 0,
        dishImg:obj.dishImg,
        dishPrice:obj.dishPrice,
      };
    });
  });

  const numArr = [];
  numList.split(";").forEach((ele) => {
    numArr.push(ele.split(","));
  });

  respArr.forEach((ele, idx) => {
    ele.forEach((item, index) => {
      item.num = numArr[idx][index];
    });
  });

  new respData(ctx, respArr, 200).respFn();
});

//后台更新订单状态
router.post("/dishs/updateDishsOrder", async (ctx) => {
  const { id ,orderStatus} = ctx.request.body;
  const updateSql = "update Dishs_order set orderStatus=? where id=?";
  const updateParams = [orderStatus,id];
    try {
        const update_data = await query(updateSql, updateParams);
        new respData(ctx, '订单状态修改成功', 200).respFn();
    } catch (error) {
        new respData(ctx, '订单状态修改失败', 201).respFn();
    }


});

module.exports = router.routes();
