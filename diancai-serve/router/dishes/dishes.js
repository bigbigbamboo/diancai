const router = require("koa-router")();
const handel = require("../../tool/handel");
const respData = require("../../tool/respData");
const { getToken, addUrl, queryUrl, delUrl, updateUrl } = require("../../tool/getToken");
const { taketoken } = require("../../token/jwt");
const { Auth } = require("../../token/auth");
const { checkParams, checkLogin } = require("../../tool/checkParams");
const { GetTime } = require('../../tool/getTime')
const { query } = require("../../MYSQL/query");
/**s
 * 菜品分类接口
 *  id categroyId categoryName  createTime
 * 用户只要上传对应categoryName
 */
router.post("/dishCategory/CreateDishesCategory", async (ctx) => {
    new checkParams(ctx.request.body).checkParamIsNull()
    const { categoryName } = ctx.request.body
    const timeId = new Date().getTime()
    const categroyId = 'a' + timeId
    const createTime = new GetTime().getTimeFn()
    const sql = `select * from Dishes_category where categoryName=?`
    const queryParmas = [categoryName]
    const sql_data = await query(sql, queryParmas)
    if (sql_data.length > 0) throw new handel('该商品类目已存在', 201)
    else {
        const addSql = 'insert into Dishes_category(categoryName,categroyId,createTime) values(?,?,?)'
        const addSqlParams = [categoryName, categroyId, createTime]
        const sql_data = await query(addSql, addSqlParams)
        throw sql_data ? new handel('添加成功', 200) : new handel('添加失败', 500)
    }
});

router.get("/dishCategory/queryDishesCategory", async (ctx) => {
    try {
        const sql = `select * from Dishes_category`
        const sql_data = await query(sql)
        new respData(ctx, { list: sql_data, total: sql_data.length }, 200).respFn()
    } catch (error) {
        throw new handel(error, 500)
    }
})

router.post("/dishCategory/deleteDishesCategory", async (ctx) => {
    new checkParams(ctx.request.body).checkParamIsNull()

    const { id } = ctx.request.body 
    try {
        const delSql = 'delete from Dishes_category where id=?'
        const delParams = [id]
        const sql_data = await query(delSql,delParams)
        console.log(sql_data)
        sql_data 
        ? new respData(ctx, { msg: '删除成功' }, 200).respFn()
        : new respData(ctx, { msg: '删除失败' }, 201).respFn()
    } catch (error) {
        new respData(ctx, { msg: '删除失败' }, 201).respFn()
    }
})

module.exports = router.routes();
