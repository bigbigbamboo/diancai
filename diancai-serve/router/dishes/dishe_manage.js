const router = require("koa-router")();
const handel = require("../../tool/handel");
const respData = require("../../tool/respData");
const {getToken,addUrl,queryUrl,delUrl,updateUrl} = require("../../tool/getToken");
const { taketoken } = require("../../token/jwt");
const { Auth } = require("../../token/auth");
const { checkParams, checkLogin } = require("../../tool/checkParams");
const {GetTime} = require('../../tool/getTime')
const {query} = require("../../MYSQL/query");

/**
 * 菜品管理接口__创建
 *   createTime dishCategory dishName dishImg dishPrice
 *  
 */
router.post("/dishsMange/createDish",new Auth().m, async (ctx) => {
    new checkParams(ctx.request.body).checkParamIsNull()
    const createTime = new GetTime().getTimeFn()
    console.log('createTime',createTime)
    const categoryName = ctx.request.body.dishCategory
    const dishName = ctx.request.body.dishName
    const dishPrice = ctx.request.body.dishPrice
    const dishImg = ctx.request.body.dishImg

    const sql = `select * from Dishes_category` 
    const sql_data =await query(sql)
    const obj = sql_data.find(ele=>{
        return ele.categoryName === categoryName
    })
    const categroyId = obj.categroyId
    const addSql = 'insert into Dishes_manage(createTime,categroyId,categoryName,dishImg,dishName,dishPrice) values(?,?,?,?,?,?)' 
    const sqlParams=[createTime,categroyId,categoryName,dishImg,dishName,dishPrice]
     
    try {
        const data =await query(addSql,sqlParams)
        console.log(data)
        data 
        ? new respData(ctx,'添加成功',200).respFn() 
        : new respData(ctx,'添加失败',200).respFn() 
    } catch (error) {
        new respData(ctx,'添加失败',200).respFn() 
    }

});


router.post("/dishsMange/queryDish",async (ctx) => {
    const { page } = ctx.request.body 

    var sql = 'SELECT * FROM Dishes_manage'; 
    const all_data =await query(sql)

    if(page === 'all'){
        new respData(ctx,{list:all_data,total:all_data.length},200).respFn()
    }
    if(page !== 'all'){
        var start = (page - 1) * 10; 
        var sql = 'SELECT * FROM Dishes_manage limit ' + start + ',10'; 
        try { 
            const sql_data =await query(sql)
            new respData(ctx,{list:sql_data,total:all_data.length},200).respFn()
        } catch (error) {
            throw new handel(error,500)
        }
    }

 
    
    
    // let {page} = ctx.query
	// let sk = page * 10
    // const queryParams = {
    //     env:"diandan-1gxvy5tp6e2578d1",
    //     query:`db.collection('Dishes_manage').where({}).skip(${sk}).get()`
    //     }
    // try {
    //     const res =await new getToken().UserFn(queryUrl,queryParams) 
    //     const data = res.data.map(item=>{return JSON.parse(item)})
    //     if(res.errcode === 0){
    //         new respData(ctx,{list:data,total:res.pager.Total},200).respFn()
    //     }
    // } catch (error) {
    //     throw new handel(error,500)
    // }
}) 


router.post("/dishsMange/deleteDish",new Auth().m,async (ctx) => {
    new checkParams(ctx.request.body).checkParamIsNull()
    const { id } = ctx.request.body 
    try {
        const delSql = 'delete from Dishes_manage where id=?'
        const delParams = [id]
        const sql_data = await query(delSql,delParams)
        console.log(sql_data)
        sql_data 
        ? new respData(ctx, { msg: '删除成功' }, 200).respFn()
        : new respData(ctx, { msg: '删除失败' }, 201).respFn()
    } catch (error) {
        new respData(ctx, { msg: '删除失败' }, 201).respFn()
    }
    // const _id = ctx.request.body._id
    // const delParams = {
    //     env:"diandan-1gxvy5tp6e2578d1",
    //     query:`db.collection('Dishes_manage').doc('${_id}').remove({})`
    //     }
    //     // db.collection(\"geo\").where({done:false}).remove()
    // const res =await new getToken().UserFn(delUrl,delParams)  //查询 
    // console.log(res)
    // if(res.errcode === 0 && res.deleted === 1){
    //     new respData(ctx,{msg:'删除成功'},200).respFn()
    // }else{
    //     new respData(ctx,{msg:'删除失败'},201).respFn()
    // }
})



module.exports = router.routes();
