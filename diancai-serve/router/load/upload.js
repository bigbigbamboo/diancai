const router = require('koa-router')()
const result = require('../../tool/handel')
const respData = require('../../tool/respData')
const {getToken,addUrl,queryUrl,delUrl,updateUrl} = require('../../tool/getToken')
const taketoken = require('../../token/jwt')
const {Auth}=require('../../token/auth')

const {upload,cosUpload} = require('../../cos/cos')

router.post('/test/upload',upload.single('file'),async (ctx,next)=>{
    const FoldeName = ctx.request.body.FoldeName
    try {
        const res = await cosUpload(ctx.file.filename,ctx.file.path,FoldeName)
        // console.log('res',res)
        new respData(ctx,'https://'+res.Location,200).respFn()
    } catch (error) {
        new respData(ctx,'图片上传发生错误',500).respFn()
    }
})

module.exports = router.routes()