const router = require("koa-router")();
const handel = require("../../tool/handel");
const respData = require("../../tool/respData");
const { getToken,  } = require("../../tool/getToken");
const { taketoken } = require("../../token/jwt");
const { Auth } = require("../../token/auth");
const { checkParams, checkLogin } = require("../../tool/checkParams");
const { GetTime } = require("../../tool/getTime");
const { query } = require("../../MYSQL/query");
const { upload, cosUpload, cosBuffer } = require("../../cos/cos");
const { GetServe } = require('../../tool/getlastTime')
/**
 * 可视化数据分析
 *
 */
 
 

router.post("/echarts/analysisApi", async (ctx) => {  
  const timerData= new GetServe().sevenDayFn()
  // 查看全部数据
  const allsql = `select * from Dishs_order` 
  const allData = await query(allsql)
//   console.log( JSON.stringify(allData[0].createTime).substring(1,11) ) 
  const respArr = timerData.map(ele => {
      return { time:ele,salePrice:0 }
  })
  allData.forEach((ele,idx)=>{
    respArr.forEach((item,index)=>{ 
        tempEle = JSON.stringify(ele.createTime).substring(1,11)
        if(tempEle == item.time) item.salePrice += Number(ele.totalPrice)
    })
  }) 
  new respData(ctx, respArr , 200).respFn()
  
});

module.exports = router.routes();
