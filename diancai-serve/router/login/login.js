const router = require('koa-router')()
const handel = require('../../tool/handel')
const respData = require('../../tool/respData')
const {getToken,addUrl,queryUrl,delUrl,updateUrl} = require('../../tool/getToken')
const {taketoken} = require('../../token/jwt')
const {Auth}=require('../../token/auth')
const {checkParams,checkLogin} = require('../../tool/checkParams')
const {query} = require("../../MYSQL/query");

// 注册接口
router.post('/register', async ctx=>{ 
    new checkParams(ctx.request.body,['phone','password']).checkParam()
    const phone = ctx.request.body.phone
    const password = ctx.request.body.password 
    new checkLogin().start(phone,password)
    uid = new Date().getTime().toString().substring(5,14);
    const sql = `select * from userInfo where phone=${phone}` 
    const sql_data =await query(sql)
    if(sql_data.length > 0)throw new handel('账号已注册',201)

    const addSql = 'insert into userInfo(password,phone,uid) values(?,?,?)' 
    const sqlParams=[password,phone,Number(uid)]
    try {
       const data =await query(addSql,sqlParams)
       new respData(ctx,'注册成功',200).respFn()  
    } catch (error) {
        new respData(ctx,'注册失败',500).respFn()  
    }
})

// du123456789

router.post('/login',async ctx =>{ 
    new checkParams(ctx.request.body,['phone','password']).checkParam()
    const phone = ctx.request.body.phone
    const password = ctx.request.body.password 
    new checkLogin().start(phone,password)

    const sql = `select * from userInfo where phone=? and password=?` 
    const querySqlParams = [phone,password]
    
    try {
        const sql_data =await query(sql,querySqlParams)
        if(sql_data.length > 0){  
            new respData(ctx,{token:taketoken(sql_data[0].uid)},200).respFn()
        }else{ 
             new respData(ctx,'账号密码错误，或者该账号未注册',201).respFn() 
        }
    } catch (error) {
        throw new handel('请求失败',500)
    }
})


router.post('/test',async ctx=>{
    // console.log(ctx.request.body)  
    const sql = 'select * from userInfo'
    const data =await query(sql)
    console.log(data) 
})

module.exports = router.routes()