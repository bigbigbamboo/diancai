const router = require("koa-router")();
const handel = require("../../tool/handel");
const respData = require("../../tool/respData");
const {
  getToken,
  addUrl,
  queryUrl,
  delUrl,
  updateUrl,
} = require("../../tool/getToken");
const { taketoken } = require("../../token/jwt");
const { Auth } = require("../../token/auth");
const { checkParams, checkLogin } = require("../../tool/checkParams");
const { GetTime } = require("../../tool/getTime");
const { query } = require("../../MYSQL/query");
const { upload, cosUpload, cosBuffer } = require("../../cos/cos");
/**
 * 菜品管理接口__创建
 *   createTime dishCategory dishName dishImg dishPrice
 *
 */
let Code = function () {
  var orderCode = "";
  for (var i = 0; i < 6; i++) {
    orderCode += Math.floor(Math.random() * 6);
  }
  // 时间戳+orderCode
  orderCode = new Date().getTime() + orderCode;
  return orderCode + ".jpg";
};

//  Key,bufferData,FoldeName
router.post("/dish/createAppCode", async (ctx) => {
  const {  deskNum  } = ctx.request.body; 
  const data_buffer = await new getToken().getAppCodeFn(deskNum);
  const res = await cosBuffer(Code(), data_buffer, "dishs/"); 
  const createTime = new GetTime().getTimeFn()
  const url = 'https://' + res.Location
  const sql = `select * from Dishs_appCode where deskNum=?`
  const queryParmas = [deskNum]
  const sql_data = await query(sql, queryParmas)
  const respObj = { createTime,url,deskNum ,msg: "创建桌号成功",}
  if (sql_data.length > 0) throw new handel('该桌号已存在', 201)
  else {
	const addSql = 'insert into Dishs_appCode(createTime,url,deskNum) values(?,?,?)'
	const addSqlParams = [createTime,url,deskNum]
	const sql_data = await query(addSql, addSqlParams) 
	 sql_data ? new respData(ctx, respObj , 200).respFn() : new handel('创建桌号失败', 500)
}
  
});


router.post("/dish/queryAppCode", async (ctx) => { 
  const { page } = ctx.request.body 
  // 查看全部数据
  const allsql = `select * from Dishs_appCode` 
  const allData = await query(allsql)
  var start = (page - 1) * 5; 
  var sql = 'SELECT * FROM Dishs_appCode limit ' + start + ',5'; 
  
  const sql_data = await query(sql)
  new respData(ctx, {list:sql_data,totalPage:allData.length} , 200).respFn()
  
});

module.exports = router.routes();
