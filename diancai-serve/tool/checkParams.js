const handel = require('../tool/handel')
class checkParams{
    constructor(reqParams,correctParams){
        this.reqParams = reqParams
        this.correctParams = correctParams
    } 
    // 校验入参名称是否正确
    checkParam(){
        const inCorParam = Object.keys(this.reqParams).filter(ele => {
            return this.correctParams.every(item => {
                return item !== ele
            })
        });
        if(inCorParam.length>0) throw new handel('入参名称错误'+inCorParam,201)
    }
    // 入参不能为空
    checkParamIsNull(){
        for (const key in this.reqParams) { 
            if(!this.reqParams[key]) throw new handel('入参不能为空',201)
        }
    }
    // 校验手机号码
    checkPhone(phone){ 
        const reg = /^1[3456789]\d{9}$/
        if(!reg.test(phone)) throw new handel('手机号格式错误',201)
    }
    // 密码校验：6-20位数字和字母结合
	checkPassword(password){
		let reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
		if(!reg.test(password)){
			throw new handel('密码格式错误',201)
		}
	}
}

// 登录校验
 class checkLogin extends checkParams{
     start(phone,password){
         super.checkPhone(phone)
         super.checkPassword(password)
     }
 }


module.exports = {checkParams,checkLogin}