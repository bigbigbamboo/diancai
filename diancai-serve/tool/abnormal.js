
const result = require('./handel')

const abnormal = async (ctx,next) =>{
    try {
        await next()
    } catch (error) {
        // console.log('err',error)
        const isRes = error instanceof result
        if(isRes){
            ctx.body = {
                msg:error.msg,
                code:error.code
            }
        }else{
            ctx.body = {
                msg:'服务器内部错误',
                code:500
            }
        }
    }
}

module.exports = abnormal