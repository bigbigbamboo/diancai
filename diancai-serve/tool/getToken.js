const axios = require('axios')
const qs = require('querystring')


const params = qs.stringify({
    grant_type:'client_credential',
    appid:'wxea013ecd4483d52e',
    secret:'e35ee0874b872a80aea11064fc162870'
})



const url = 'https://api.weixin.qq.com/cgi-bin/token?'+params
const codeUrl = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='


class getToken {
    constructor(){
        
    }
    async respFn(){   //获取token
      let data = await axios.get(url) 
      return data.data.access_token
    }
    async UserFn(addUrl,data){ //添加记录 
        const url = addUrl + qs.stringify({access_token:await this.respFn()})
        let res = await axios.post(url,data)
        // console.log(res.data,'data')
        return res.data
    }
    async getAppCodeFn(deskNum){ 
      const token = await this.respFn()
      const url = codeUrl + token  
      let OBJ = JSON.stringify({path:'pages/admin/admin?deskNum=' + deskNum,width:1280})
      const res = await axios.post(url,OBJ,{responseType:'arraybuffer'}) 
      return res.data
    }
}

// 添加记录
const addUrl = "https://api.weixin.qq.com/tcb/databaseadd?";
// 查询记录
const queryUrl = "https://api.weixin.qq.com/tcb/databasequery?";
// 删除记录
const delUrl = "https://api.weixin.qq.com/tcb/databasedelete?";
// 更新记录
const updateUrl = "https://api.weixin.qq.com/tcb/databaseupdate?";


module.exports = {getToken,addUrl,queryUrl,delUrl,updateUrl}


// diandan-1gxvy5tp6e2578d1  