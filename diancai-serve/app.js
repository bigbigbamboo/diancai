const koa = require('koa')
const app = new koa()
const cors = require('koa2-cors')
const json = require('koa-json')
const bodyparser = require('koa-bodyparser')
const router = require('koa-router')()


const abnormal = require('./tool/abnormal')

app.use(cors())
app.use(json())
app.use(bodyparser())
app.use(abnormal)


const login = require('./router/login/login.js')
const upload = require('./router/load/upload')
const dishes = require('./router/dishes/dishes')
const dishe_manage = require('./router/dishes/dishe_manage')
const order = require('./router/dishes/order')
const appCode = require('./router/appCode/appCode')
const analysis = require('./router/analysis/analysis')

router.use('',login)
router.use('',upload)
router.use('',dishes)  //菜品分类
router.use('',dishe_manage)  //菜品管理
router.use('',order)  //订单
router.use('',appCode)  //订单
router.use('',analysis)  //可视化数据分析




app.use(router.routes()).use(router.allowedMethods()) 

app.use(async (ctx,next)=>{
    ctx.body = 'hello world'
})


app.listen(1111)