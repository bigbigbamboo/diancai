const basicAuth = require('basic-auth')
const jwt = require('jsonwebtoken')
const result = require('../tool/handel')
const security = require('./tokentime').security


class Auth{
    constructor(){}
    // get取值函数  set存值函数
    get m(){
        return async (ctx,next)=>{  
            const token = ctx.request.header.token
            if(!token){
                throw new result('token不存在',401)
            }
            try {
                var authcode = jwt.verify(token,security.secretkey)
            } catch (error) {
                throw new result('token错误，没有访问权限',401)
            }
            ctx.auth = { uid: authcode.uid}
            await next()
        }
    }
    fn(){
        console.log(456)
    }
}
module.exports = {
    Auth
}