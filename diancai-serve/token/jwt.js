
const jwt = require('jsonwebtoken')
const security = require('./tokentime').security

const taketoken =(uid,scope=2)=>{
    const secretkey = security.secretkey
    const expiresIn = security.expiresIn
    const token = jwt.sign({uid,scope},secretkey,{expiresIn})
    // console.log(token)
    return token
}
module.exports = {
    taketoken
}