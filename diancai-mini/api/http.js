const baseUrl = 'http://127.0.0.1:1111'
const http = (url,data={},method='post')=>{
	return new Promise((resolve,reject)=>{
		uni.request({
		    url:baseUrl+url, //仅为示例，并非真实接口地址。
		    data,
			method,
		    success: (res) => {
		        resolve(res.data)
		    },
			fail: (err) => {
			    reject(err)
			}
		});
	})
}
export { http }