import Vue from 'vue'
import App from './App'
import GoEasy from 'goeasy'

import { http } from './api/http.js'

import store from './store/index.js'

Vue.config.productionTip = false

App.mpType = 'app'

//全局定义Http请求方法
Vue.prototype.$http = http

//消息推送全局定义方法
Vue.prototype.goeasy = GoEasy.getInstance({
    host:"hangzhou.goeasy.io",  //若是新加坡区域：singapore.goeasy.io
    appkey:"BC-ce2f321ee7344a45a0756edb2e163067",
    modules:['pubsub']//根据需要，传入‘pubsub’或'im’，或数组方式同时传入
});


const app = new Vue({
	store,
    ...App
})
app.$mount()
