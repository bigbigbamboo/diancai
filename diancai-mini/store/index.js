import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import dishs from './modules/dish.js'
import getters from './getters.js'

export default new Vuex.Store({
  state: {
  },
  getters,
  modules: {
    dishs
  }
})
