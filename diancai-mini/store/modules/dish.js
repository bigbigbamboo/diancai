export default {
	state:{
		carsData:[]
	},
	mutations:{
		updateCarData(state,val){
			state.carsData = val
		}
	}
}